import './App.css';
import { Route, Switch } from "react-router-dom";
import { routes } from './routes/routes';

import Aside from './components/Navigation/Aside';
import Banner from './components/Banner';
import Header from './components/Header';
import Footer from './components/Footer';
import Main from './components/Main';
import Nav from './components/Navigation/Nav';

import AboutUsPage from './components/Navigation/Pages/AboutUs/AboutUsPage';
import HomePage from './components/Navigation/Pages/Home/HomePage';
import LandlordsPage from './components/Navigation/Pages/LandlordsPage/LandlordsPage';
import Login from './components/Navigation/Pages/User/Login/Login';
import RegistrationPage from './components/Navigation/Pages/User/Registration/RegistrationPage';
import SearchAdvPage from './components/Navigation/Pages/Advs/SearchAdvPage';

function App() {
  return (
    <div className="App">
        <Header>
          <Nav/>
          <Banner />
        </Header>
        <Main>
          <Aside />
          <Switch>
            <Route path={`${routes.r_aboutUs}`}>
              <AboutUsPage />
            </Route>
            <Route path={`${routes.r_advertisements}`}>
              <SearchAdvPage />
            </Route>
            <Route path={`${routes.r_landlords}`}>
              <LandlordsPage />
            </Route>
            <Route path={`${routes.r_login}`}>
              <Login />
            </Route>

            <Route path={`${routes.r_madvertisement}`}>
              Mis Anuncios
            </Route>
            <Route path={`${routes.r_mprofile}`}>
              Mi Perfil
            </Route>
            <Route path={`${routes.r_mproperties}`}>
              Mis Propiedades
            </Route>
            <Route path={`${routes.r_mreservations}`}>
              Mis Reservas
            </Route>
            <Route path={`${routes.r_mreviews}`}>
              Mis Reseñas
            </Route>

            <Route path={`${routes.r_registration}`}>
              <RegistrationPage />
            </Route>
            <Route path={`${routes.r_home}`}>
              <HomePage />
            </Route>
          </Switch>
        </Main>
      <Footer />
    </div>
  );
}

export default App;
