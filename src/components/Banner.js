import banner from '../assets/banner/banner.jpg'

const Banner = () => {

    return(
        <p>
            <img className="isBanner" src={banner} alt="banner" />
        </p>
    )
}

export default Banner