const Header = ({children}) => {

    return(
        <header className='isHeader'>
            {children}
        </header>
    )
}

export default Header