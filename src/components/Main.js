const Main = ({children}) => {

    return(
        <main className='isMain'>
            {children}
        </main>
    )
}

export default Main