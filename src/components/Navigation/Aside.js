import { useState } from "react"
import { NavLink } from "react-router-dom"
import { routes } from "../../routes/routes"

const Aside = () => {

    const user = true
    const [slide, setSlide] = useState(false)
    return(
        <aside className="sidebar unfolded-sidebar">
            <span  onClick={() => setSlide(!slide)}>
                {slide ? 'Abrir':'Cerrar'}
            </span>
            <NavLink to={`${routes.r_mprofile}`}>
                Perfil
            </NavLink>
            <NavLink to={`${routes.r_properties}`}>
                Inmuebles
            </NavLink>
            <NavLink to={`${routes.r_advertisements}`}>
                Anuncios
            </NavLink>
            <NavLink to={`${routes.r_reservations}`}>
                Reservas
            </NavLink>
            <NavLink to={`${routes.r_reviews}`}>
                Reseñas
            </NavLink>
        </aside>
    )
}

export default Aside