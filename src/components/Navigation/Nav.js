import { Link, NavLink } from "react-router-dom"
import { routes } from "../../routes/routes"
import logo from "../../assets/logo.svg"
import ClickImg from "../Utils/ClickImg"
import RegistrationPage from "./Pages/User/Registration/RegistrationPage"
import Login from "./Pages/User/Login/Login"

const Nav = () => {

    const user = false;
    return(
        <>
            <nav>
                <ClickImg img={logo} url={routes.r_home}/>
                <NavLink to={`${routes.r_advertisements}`}>
                    Búsqueda
                </NavLink>
                <NavLink to={`${routes.r_landlords}`}>
                    Caseros
                </NavLink>
                <NavLink to={`${routes.r_aboutUs}`}>
                    Conócenos
                </NavLink>
                {user &&
                    <NavLink to={`${routes.r_login}`}>
                        Iniciar Sesión
                    </NavLink>
                }
                {!user &&
                    <NavLink to={`${routes.r_registration}`}>
                        Registro
                    </NavLink>
                }
            </nav>
            <hr />
        </>
    )
}

export default Nav