import { useState } from "react"

const AdvSearchForm = () =>{
    const [query,setQuery] = useState(false)
    const [calle, setCalle] = useState('')
    const [ciudad, setCiudad] = useState('')

    const handleSubmit = async e => {
        e.preventDefault()


        setQuery('a');
        console.log(query);
    }

    return(
        <form action="GET" onSubmit={handleSubmit}>
            <div className="obfuscation">
            <div className="searchContainer">
                <label htmlFor="calle">
                    Calle: 
                    <input type="text" id="calle" name="calle" value={calle} onChange={(e) => setCalle(e.target.value)}/>
                </label>
                <label htmlFor="ciudad">
                    Ciudad: 
                    <input type="text" id="ciudad" name="ciudad" value={ciudad} onChange={(e) => setCiudad(e.target.value)}/>
                </label>
                <div className="searchButton">
                    <button>Buscar</button>
                </div>
            </div>
            </div>
        </form>
    )
}

export default AdvSearchForm