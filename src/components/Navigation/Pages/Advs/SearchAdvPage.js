import { useState } from "react"
import AdvSearchForm from "./AdvSearchForm"
import AdvSearchRes from "./AdvSearchRes"


const SearchAdvPage = () => {

    const [query, setQuery] = useState(false)

    return(
        <main>
            <AdvSearchForm query={query} setQuery={setQuery} />
            <hr />
            {query &&
                <AdvSearchRes query={query} setQuery={setQuery} />
            }
        </main>
    )
}
export default SearchAdvPage