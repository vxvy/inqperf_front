import { useState } from "react"
import { Redirect } from "react-router-dom"
import { back } from '../../../../../routes/routes'

const RegistrationPage = () => {

    const [user, setUser] = useState()
    const [isError, setError] = useState()
    const [username, setUsername] = useState()
    const [password, setPassword] = useState()
    const [confirmPassword, setConfirmPassword] = useState()
    const [tipo, setTipo] = useState()

    const handleSubmit = async e => {
        e.preventDefault()

        const res = await fetch(back.b_user, {
            method: 'POST',
            body: JSON.stringify(),
            headers: {'Content-Type':'application/json'}
        })
        if(res.ok){
            const data = await res.json()
            setUser({
                "user_uuid": "01941423-05b4-4d40-9053-e6c78babade6",
                "id_usuario": 10,
                "username": "pimpollo",
                "email": "noesunmail@gmail.com",
                "tipo": "CASERO",
                "activated_at": "2021-08-21T16:37:02.000Z",
                "activated_code": "asdf",
                "modified_at": null,
                "deleted_at": null,
                "avatar": "/uploadAvatars/default-avatar.png",
                "puntuacion_media": "0.00"
            })
            //envía mensaje de confirmación
        }else{
            setError(res.error)
        }

        console.log('a');
    }

    if(!user){

        return(
            <form action="POST" onSubmit={handleSubmit}>
                <label htmlFor="username">
                    Username: 
                    <input type="text" name="username" id="username" />
                </label>
                <label htmlFor="username">
                    Email: 
                    <input type="email" name="email" id="email" />
                </label>
                <label htmlFor="password">
                    Password: 
                    <input type="password" name="password" id="password" />
                </label>
                <label htmlFor="confirmPassword">
                    Repite la contraseña: 
                    <input type="password" name="confirmPassword" id="confirmPassword" />
                </label>
                <label htmlFor="tipo">
                    Tipo: 
                    <select name="tipo" id="tipo" value={`a`} onChange={e => setTipo(e.target.value) }>
                        <option value="INQUILINO" selected>Inquilino</option>
                        <option value="CSASERO" >Casero</option>
                        <option value="INQUILINO/CASERO" >Inquilino/Casero</option>
                        {user?.user?.tipo === 'ADMIN' &&
                            <option value="ADMIN" >Admin</option>
                        }
                    </select>
                </label>
            </form>
    )}
    else{
        <div>
            Ya estás registrado, si no has conseguido activar la cuenta ponte en contacto con el administrador
        </div>
    }
}

export default RegistrationPage