import { useState } from "react"
import { NavLink } from "react-router-dom"

const ClickImg = ({img, url}) => {
    const [logoImg, setLogoImg] = useState(img)

    return logoImg &&(
        <NavLink to={url}>
            <img className="logo" src={logoImg} alt="logo" />
        </NavLink>
    )
}

export default ClickImg