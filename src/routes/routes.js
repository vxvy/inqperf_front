const port = 3001
const host = `http://127.0.0.1`
const head = `${port}:${host}`

export const routes = {
    r_aboutUs: `/conocenos`,
    r_advertisements: `/anuncios`,
    r_home:`/`,
    r_landlords: `/caseros`,
    r_login: `/login`,
    r_madvertisement:`/misanuncios`,
    r_mprofile:`/miperfil`,
    r_mproperties:`/mispisos`,
    r_mreservations:`/misreservas`,
    r_mreviews:`/misreviews`,
    r_profile: `/perfil`,
    r_properties: `/pisos`,
    r_reservations: `/reservas`,
    r_reviews: `/resenas`,
    r_registration:`/registro`,
}

export const back = {
    b_user : `${head}/api/users`,

    b_userSelf : `${head}/api/users/:username`,
    b_userSelfAdvs : `${head}/api/user/:username/advertisements`,
    b_userSelfProps : `${head}/api/user/:username/properties`,
    b_userSelfReservs : `${head}/api/user/:username/reservations`,
    b_userSelfReviews : `${head}/api/user/:username/reviews`,

    b_props : `${head}/api/properties`,
    b_propByProp : `${head}/api/properties/:inmueble_uuid`,

    b_reservs : `${head}/api/reservations`,
    b_reservByReserv : `${head}/api/reservations/:reserva_uuid`,

    b_reviews : `${head}/api/reviews`,
    b_reviewByReview : `${head}/api/reviews/:resena_uuid`,

    b_activation : `${head}/activation`,
    b_login : `${head}/login`
}